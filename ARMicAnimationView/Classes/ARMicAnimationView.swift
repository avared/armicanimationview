//
//  ARMicAnimationView.swift
//  ARMicAnimationView
//
//  Created by Avadhut.redekar on 19/09/17.
//


import UIKit

@available(iOS 10.0, *)
class ARMicAnimationView: UIView {
    
    
    @objc public var doneButtonAction : (()-> ())?
    
    @IBInspectable var micButtonColor : UIColor = UIColor.red{
        didSet{
            self.micButton.backgroundColor = micButtonColor
        }
    }
    
    @IBInspectable var animatingCircleViewColor: UIColor = UIColor.red.withAlphaComponent(0.6){
        didSet{
            self.createBorder(aView : animatingView1)
            self.createBorder(aView : animatingView2)
        }
    }
    
    @IBInspectable var voiceLevelIndiacatorColor : UIColor = UIColor.red.withAlphaComponent(0.6){
        didSet{
            self.voiceLevelIndicatorView.backgroundColor = voiceLevelIndiacatorColor
        }
    }
    
    
    public var voiceAmplitude : Float = -121{
        didSet{
            self.animateVoiceLevelIndicatorView()
        }
    }
    
    fileprivate let animatingView1 = UIView()
    fileprivate let animatingView2 = UIView()
    fileprivate let voiceLevelIndicatorView = UIView()
    fileprivate let micButton = UIButton() //UIImageView(image: #imageLiteral(resourceName: "mic_large"))
    
    
    fileprivate let initialTransform = CGAffineTransform(scaleX: 0.28, y: 0.28)
    fileprivate let endTransform = CGAffineTransform(scaleX: 1, y: 1)
    
    override func draw(_ rect: CGRect) {
        self.setUpUI()
    }
    
    fileprivate var animationStarted = false
    @objc private func didTapDoneButton(_ button : UIButton?){
        self.stopAnimation(){self.doneButtonAction?()}
    }
    
    private func setUpUI(){
        let animatingViewSideLength = self.bounds.size.width >= self.bounds.size.height ? self.bounds.size.height : self.bounds.size.width
        
        self.setUp(aView: animatingView1, sideLength: animatingViewSideLength)
        self.setUp(aView: animatingView2, sideLength: animatingViewSideLength)
        self.setUp(aView: voiceLevelIndicatorView, sideLength: 200)
        self.setUp(aView: micButton, sideLength: 75)
        self.setUpMicButton()
    }
    
    private func setUp(aView : UIView, sideLength: CGFloat){
        aView.frame = CGRect(x: 0, y: 0, width: sideLength, height: sideLength)
        aView.center = CGPoint(x: self.bounds.size.width/2, y: self.bounds.size.height/2)
        aView.layer.cornerRadius = sideLength/2
        self.addSubview(aView)
        guard !(aView is UIButton) else { return}
        aView.transform = initialTransform
    }
    
    private func setUpMicButton(){
        micButton.setTitle("", for: .normal)
        
        micButton.addTarget(self, action: #selector(self.didTapDoneButton), for: .touchUpInside)

    }
    
    private func createBorder(aView : UIView){
        aView.layer.borderColor = self.animatingCircleViewColor.cgColor
        aView.layer.borderWidth = 2.0
    }
}

@available(iOS 10.0, *)
extension ARMicAnimationView{  //extension for Animation methods
    public func startAnimation(){
        guard !animationStarted else { return }
        
        animationStarted = true
        self.animateCircle(view: animatingView1, duration: 3, delay: 0) {}
        self.animateCircle(view: animatingView2, duration: 3, delay: 0.2) {
            guard self.animationStarted else { return }
            self.continuousAnimation()
        }
    }
    
    public func stopAnimation(completion : ()->())
    {
        animationStarted = false
        completion()
    }
    
    private func continuousAnimation(){
        self.animateCircle(view: animatingView2, duration: 1.5, delay: 0) {
            guard self.animationStarted else { return }
            self.continuousAnimation()
        }
    }
    
    private func animateCircle(view : UIView, duration : TimeInterval, delay: TimeInterval, completion : @escaping ()-> ()){
        
        UIView.animate(withDuration: duration, delay: delay, options: UIViewAnimationOptions.curveEaseOut, animations: {
            self.changeCircleParams(ofView: view, initial: false)
            
        }) { (stop) in
            self.changeCircleParams(ofView: view, initial: true)
            completion()
        }
    }
    
    private func changeCircleParams(ofView : UIView, initial : Bool){
        ofView.transform = initial ? self.initialTransform : self.endTransform
        ofView.alpha = initial ? 1.0 : 0.0
    }
    
    fileprivate func animateVoiceLevelIndicatorView(){
        
        let lowerPowerLimit :Float = -120   //power range= -160 to 0
        
        guard self.window != nil && self.animationStarted && voiceAmplitude > lowerPowerLimit else{
            self.voiceLevelIndicatorView.transform =  initialTransform
            return
        }
        
        let positivePower = (lowerPowerLimit * -1) + voiceAmplitude
        let proportion = positivePower / 200 + 0.2 //proportion should be above 0.4, so view would be shown
        
        UIView.animate(withDuration: 0.01, animations: {
            self.voiceLevelIndicatorView.transform =  CGAffineTransform(scaleX: CGFloat(proportion), y: CGFloat(proportion))
        })
    }
}

