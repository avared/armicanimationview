# ARMicAnimationView

[![CI Status](http://img.shields.io/travis/Avadhut.redekar/ARMicAnimationView.svg?style=flat)](https://travis-ci.org/Avadhut.redekar/ARMicAnimationView)
[![Version](https://img.shields.io/cocoapods/v/ARMicAnimationView.svg?style=flat)](http://cocoapods.org/pods/ARMicAnimationView)
[![License](https://img.shields.io/cocoapods/l/ARMicAnimationView.svg?style=flat)](http://cocoapods.org/pods/ARMicAnimationView)
[![Platform](https://img.shields.io/cocoapods/p/ARMicAnimationView.svg?style=flat)](http://cocoapods.org/pods/ARMicAnimationView)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

ARMicAnimationView is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'ARMicAnimationView'
```

## Author

Avadhut.redekar, avadhut.redekar@landmarkgroup.in

## License

ARMicAnimationView is available under the MIT license. See the LICENSE file for more info.
